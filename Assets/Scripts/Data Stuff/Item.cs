﻿using TMPro;
using UnityEngine;

namespace Data_Stuff
{
    [CreateAssetMenu(fileName = "New Item", menuName = "Data Stuff/Item", order = 0)]
    public class Item : ScriptableObject
    {
        public Sprite iconSprite;
        [Range(0f, 100f)] public int defaultPercent;
        public string itemName;
        public string description;
        public Color color;
    }
}