﻿using System;
using System.Collections.Generic;
using System.Linq;
using Data_Stuff;
using UnityEngine;

namespace Mechanics
{
    public class Items : MonoBehaviour
    {
        [SerializeField] private Transform parent;
        [SerializeField] private Transform createWindow;
        [SerializeField] private List<Item> dataItems;
        [SerializeField] private Item defaultItem;
        [SerializeField] private GameObject itemPrefab;
        [SerializeField] private GameObject newItemPrefab;

        private readonly Dictionary<string, List<GameObject>> _items = new Dictionary<string, List<GameObject>>(4)
        {
            {"items", new List<GameObject>()},
            {"itemClickPanels", new List<GameObject>()},
            {"itemDeletePanels", new List<GameObject>()},
            {"itemSwapPanels", new List<GameObject>()}
        };

        // private readonly List<GameObject> _itemClickPanels = new List<GameObject>();
        // private readonly List<GameObject> _itemDeletePanels = new List<GameObject>();
        private Camera _camera;
        private Vector2 _viewSize;
        private int _selectedItemId = -1;
        private int _secondSelectedItemId;

        private GameObject _newItemObject;

        private void Start()
        {
            if (Camera.main != null)
                _camera = Camera.main;
            var orthographicSize = _camera.orthographicSize;
            // half screen
            _viewSize = new Vector2(orthographicSize * _camera.aspect, orthographicSize);

            for (int i = 0; i < dataItems.Count; i++)
            {
                _items["items"].Add(Instantiate(itemPrefab, parent));
                _items["items"][i].GetComponent<ItemSettings>().Configure(dataItems[i]);
                _items["items"][i].name = $"item {i}";
                _items["itemClickPanels"].Add(_items["items"][i].GetComponent<ItemSettings>().clickPanel);
                _items["itemDeletePanels"].Add(_items["items"][i].GetComponent<ItemSettings>().removePanel);
                _items["itemSwapPanels"].Add(_items["items"][i].GetComponent<ItemSettings>().swapPanel);
            }

            ItemPositionsConfigure();
        }

        public void ActivateAllClickPanels()
        {
            foreach (var clickPanel in _items["itemClickPanels"]
                .Where(clickPanel => clickPanel.activeSelf is false))
                clickPanel.SetActive(true);
        }

        public void CreateItem()
        {
            Item newItem = ScriptableObject.CreateInstance<Item>();
            newItem.color = Color.gray;
            dataItems.Add(newItem);
        }

        public void CreateDefaultItem()
        {
            _newItemObject = Instantiate(newItemPrefab, createWindow);
            Item newDefItem = Instantiate(defaultItem, createWindow);
            _newItemObject.GetComponent<ItemSettings>().Configure(newDefItem);
            _newItemObject.GetComponent<NewItemConfigure>().ConfigureItem(newDefItem);
        }

        public void AddItem()
        {
            // Debug.Log($"item name {_newItemObject.GetComponent<ItemSettings>()._item.itemName}");
            DestroyNewItem();
            dataItems.Add(_newItemObject.GetComponent<NewItemConfigure>().newItem);
            _items["items"].Add(Instantiate(itemPrefab, parent));
            _items["items"].Last().GetComponent<ItemSettings>().Configure(dataItems.Last());
            _items["itemClickPanels"].Add(_items["items"].Last().GetComponent<ItemSettings>().clickPanel);
            _items["itemDeletePanels"].Add(_items["items"].Last().GetComponent<ItemSettings>().removePanel);
            _items["itemSwapPanels"].Add(_items["items"].Last().GetComponent<ItemSettings>().swapPanel);
            _items["items"].Last().name = $"item {_items["items"].Count - 1}";

            RenameAllItems();
            ItemPositionsConfigure();

            AllDeletePanelsActivator(false);
        }

        public void DestroyNewItem()
        {
            Destroy(_newItemObject, 0.1f);
        }

        public void SaveCurrentItemId(int id)
        {
            Debug.Log($"first {_selectedItemId}, sec {_secondSelectedItemId}");
            
            if (_selectedItemId == -1)
                _selectedItemId = id;
            else
            {
                _secondSelectedItemId = id;
                SwapTwoItems();
                _selectedItemId = -1;
                
                Debug.Log("i'm here");
            }
        }

        public void RemoveItem()
        {
            dataItems.RemoveAt(_selectedItemId);
            Destroy(_items["items"][_selectedItemId]);
            _items["items"].RemoveAt(_selectedItemId);
            _items["itemClickPanels"].RemoveAt(_selectedItemId);
            _items["itemDeletePanels"].RemoveAt(_selectedItemId);
            _items["itemSwapPanels"].RemoveAt(_selectedItemId);

            _selectedItemId = -1;
            RenameAllItems();
            ItemPositionsConfigure();
            AllDeletePanelsActivator(false);
        }

        private void SwapTwoItems()
        {
            Item currentItem = dataItems[_selectedItemId];
            dataItems[_selectedItemId] = dataItems[_secondSelectedItemId];
            dataItems[_secondSelectedItemId] = currentItem;

            GameObject current = _items["items"][_selectedItemId];
            Debug.Log(current.name);
            
            _items["items"][_selectedItemId] = _items["items"][_secondSelectedItemId];
            _items["items"][_secondSelectedItemId] = current;

            string newName = _items["items"][_secondSelectedItemId].name;
            _items["items"][_secondSelectedItemId].name = _items["items"][_selectedItemId].name;
            _items["items"][_selectedItemId].name = newName;

            // Debug.Log("_selectedItemId "+_items["items"][_selectedItemId].name);
            // Debug.Log($"selected {_selectedItemId}, second sel {_secondSelectedItemId}");
            Debug.Log($"first name {_items["items"][_selectedItemId].name}, second name {_items["items"][_secondSelectedItemId].name}");
            
            ItemPositionsConfigure();
            AllSwapPanelsActivator(false);
        }

        private void RenameAllItems()
        {
            for (var i = 0; i < _items["items"].Count; i++)
                _items["items"][i].name = $"item {i}";
        }

        public void AllDeletePanelsActivator(bool activate)
        {
            foreach (var deletePanel in _items["itemDeletePanels"])
            {
                if(deletePanel)
                    deletePanel.SetActive(activate);
            }
        }
        
        public void AllSwapPanelsActivator(bool activate)
        {
            foreach (var swapPanel in _items["itemSwapPanels"])
            {
                if(swapPanel)
                    swapPanel.SetActive(activate);
            }
        }

        private void ItemPositionsConfigure()
        {
            const float twoThirds = .666f;
            switch (dataItems.Count)
            {
                case 1:
                    _items["items"][0].transform.position = Vector3.zero;
                    break;
                case 2:
                    _items["items"][0].transform.position = new Vector2(-_viewSize.x / 2, 0);
                    _items["items"][1].transform.position = new Vector2(_viewSize.x / 2, 0);
                    break;
                case 3:
                    _items["items"][0].transform.position = new Vector2(-_viewSize.x * twoThirds, 0);
                    _items["items"][1].transform.position = Vector3.zero;
                    _items["items"][2].transform.position = new Vector2(_viewSize.x * twoThirds, 0);
                    break;
                case 4:
                    _items["items"][0].transform.position = new Vector2(-_viewSize.x / 2, _viewSize.y / 2);
                    _items["items"][1].transform.position = new Vector2(_viewSize.x / 2, _viewSize.y / 2);
                    _items["items"][2].transform.position = new Vector2(_viewSize.x / 2, -_viewSize.y / 2);
                    _items["items"][3].transform.position = new Vector2(-_viewSize.x / 2, -_viewSize.y / 2);
                    break;
                case 5:
                    _items["items"][0].transform.position = new Vector2(-_viewSize.x / 2, _viewSize.y / 2);
                    _items["items"][1].transform.position = new Vector2(_viewSize.x / 2, _viewSize.y / 2);
                    _items["items"][2].transform.position = new Vector2(_viewSize.x / 2, -_viewSize.y / 2);
                    _items["items"][3].transform.position = new Vector2(-_viewSize.x / 2, -_viewSize.y / 2);
                    _items["items"][4].transform.position = Vector3.zero;
                    break;
                case 6:
                    _items["items"][0].transform.position = new Vector2(-_viewSize.x * twoThirds, _viewSize.y / 2);
                    _items["items"][1].transform.position = new Vector2(0, _viewSize.y / 2);
                    _items["items"][2].transform.position = new Vector2(_viewSize.x * twoThirds, _viewSize.y / 2);
                    _items["items"][3].transform.position = new Vector2(-_viewSize.x * twoThirds, -_viewSize.y / 2);
                    _items["items"][4].transform.position = new Vector2(0, -_viewSize.y / 2);
                    _items["items"][5].transform.position = new Vector2(_viewSize.x * twoThirds, -_viewSize.y / 2);
                    break;
                case 7:
                    _items["items"][0].transform.position = new Vector2(-_viewSize.x * twoThirds / 2, _viewSize.y / 2);
                    _items["items"][1].transform.position = new Vector2(_viewSize.x * twoThirds / 2, _viewSize.y / 2);
                    _items["items"][2].transform.position = new Vector2(-_viewSize.x * twoThirds, 0);
                    _items["items"][3].transform.position = Vector3.zero;
                    _items["items"][4].transform.position = new Vector2(_viewSize.x * twoThirds, 0);
                    _items["items"][5].transform.position = new Vector2(-_viewSize.x * twoThirds / 2, -_viewSize.y / 2);
                    _items["items"][6].transform.position = new Vector2(_viewSize.x * twoThirds / 2, -_viewSize.y / 2);
                    break;
                default:
                    Debug.LogWarning(dataItems.Count);
                    break;
            }
        }
    }
}