﻿using UnityEngine;

namespace Mechanics
{
    public class Input : MonoBehaviour
    {
        private InputControls _input;
        private CameraZoom _cameraZoom;
        private Items _items;
        public GameObject confirmBox;
        [SerializeField] private GameObject createItemBox;
        
        private void OnEnable() => _input.Enable();
        private void OnDisable() => _input.Disable();

        private void Awake()
        {
            _input = new InputControls();
            _cameraZoom = FindObjectOfType<CameraZoom>();
            _items = FindObjectOfType<Items>();
            
            _input.UI.Cancel.started += ctx =>
            {
                _cameraZoom.SetZoom(false);
                _items.ActivateAllClickPanels();

                if (confirmBox.activeSelf)
                {
                    confirmBox.SetActive(false);
                    _items.AllDeletePanelsActivator(false);
                }
                if (createItemBox.activeSelf)
                {
                    createItemBox.SetActive(false);
                    _items.DestroyNewItem();
                }
            };
        }

        public void OpenCreateItemWindow()
        {
            createItemBox.SetActive(true);
        }
    }
}