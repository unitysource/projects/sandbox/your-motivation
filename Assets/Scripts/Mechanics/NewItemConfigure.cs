﻿using System;
using Data_Stuff;
using UnityEngine;

namespace Mechanics
{
    public class NewItemConfigure : MonoBehaviour
    {
        [HideInInspector] public Item newItem;

        public void ConfigureItem(Item item)
        {
            newItem = item;
        }

        public void GetName(string str)
        {
            newItem.itemName = str;
        }
        
        public void GetDescription(string str)
        {
            newItem.description = str;
        }
        
        
    }
}