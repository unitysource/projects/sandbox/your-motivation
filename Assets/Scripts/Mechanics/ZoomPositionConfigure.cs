﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Mechanics
{
    public class ZoomPositionConfigure : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private Transform parentTransform;
        
        public void OnPointerClick(PointerEventData eventData)
        {
            CameraZoom zoom = FindObjectOfType<CameraZoom>();
            zoom.target = parentTransform.position;
            zoom.IsZoom = true;
        }
    }
}