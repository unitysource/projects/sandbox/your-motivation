﻿using System.Collections.Generic;
using UnityEngine;

namespace Mechanics
{
    public class CameraZoom : MonoBehaviour
    {
        [SerializeField] private float speed;
        [SerializeField] private float zoomSpeed;
        [SerializeField] private new BoxCollider2D collider;
        [SerializeField] private float zoomOrtSize = 3f;
        public bool IsZoom { get; set; }
        [HideInInspector] public Vector3 target = Vector3.zero;

        private float _ortSize;
        private Vector3 _defaultTarget;
        private Camera _camera;
        private float _zOffset;
        private Vector2 _viewSize;

        private void Start()
        {
            _defaultTarget = Vector3.zero;

            if (Camera.main != null)
                _camera = Camera.main;
            _zOffset = _camera.transform.position.z;
            var orthographicSize = _camera.orthographicSize;
            _ortSize = orthographicSize;

            _viewSize = new Vector2(orthographicSize * _camera.aspect, orthographicSize);
        }

        private void FixedUpdate()
        {
            var cameraPosition = _camera.transform.position;

            // var bounds = collider.bounds;
            // if(Math.Abs(targetPosition.x - cameraPosition.x) < .1f)
            //     return;

            if (IsZoom)
            {
                cameraPosition = Vector3.Lerp(cameraPosition,
                    new Vector3(target.x, target.y, _zOffset),
                    speed * Time.deltaTime);
                _camera.orthographicSize =
                    Mathf.Lerp(_camera.orthographicSize, zoomOrtSize, zoomSpeed * Time.deltaTime);
            }
            else
            {
                cameraPosition = Vector3.Lerp(cameraPosition,
                    new Vector3(_defaultTarget.x, _defaultTarget.y, _zOffset),
                    speed * Time.deltaTime);
                _camera.orthographicSize = 
                    Mathf.Lerp(_camera.orthographicSize, _ortSize, zoomSpeed * Time.deltaTime);
            }

            // cameraPosition = new Vector3(
            //     Mathf.Clamp(cameraPosition.x, bounds.min.x + _viewSize.x,
            //         bounds.max.x - _viewSize.x),
            //     Mathf.Clamp(cameraPosition.y, bounds.min.y + _viewSize.y,
            //         bounds.max.y - _viewSize.y), _zOffset);

            _camera.transform.position = cameraPosition;
        }

        public void SetZoom(bool zoom) =>
            IsZoom = zoom;
    }
}