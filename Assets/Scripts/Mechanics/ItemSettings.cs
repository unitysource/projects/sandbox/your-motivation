﻿using System;
using System.Collections;
using System.Globalization;
using System.Linq;
using Data_Stuff;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static System.Int32;
using static System.String;

namespace Mechanics
{
    public class ItemSettings : MonoBehaviour
    {
        
        [SerializeField] private Image icon;
        [SerializeField] private Image percentFilled;
        [SerializeField] private TextMeshProUGUI percent;
        [SerializeField] private TextMeshProUGUI percentField;
        [SerializeField] private TextMeshProUGUI itemName;
        [SerializeField] private TextMeshProUGUI itemDescription;
        [SerializeField] private Slider percentSlider;
        public GameObject clickPanel;
        public GameObject removePanel;
        public GameObject swapPanel;
        [SerializeField] private float fillSpeed = 1f;
        public float openFieldDelay = 1f;

        private Item _item;
        private Items _items;
        private GameObject _confirmBox;

        private void Start()
        {
            _confirmBox = FindObjectOfType<Input>().confirmBox;
            _items = FindObjectOfType<Items>();
        }

        public void SelectItem()
        {
            int index = Parse(gameObject.name.Split().Last());
            _items.SaveCurrentItemId(index);
        }

        public void OpenConfirmWindow()
        {
            _confirmBox.SetActive(true);
        }

        public void Configure(Item item)
        {
            _item = item;

            icon.sprite = item.iconSprite;
            icon.color = item.color;
            percent.text = item.defaultPercent + "%";
            percentFilled.fillAmount = percentSlider.value = item.defaultPercent / 100f;
            percentFilled.color = item.color;
            itemName.text = item.itemName;
            itemName.color = item.color;
            itemDescription.text = item.description;
        }
        
        // public void ItemConfigure(Item item)
        // {
        //     icon.sprite = item.iconSprite;
        //     icon.color = item.color;
        //     percent.text = item.defaultPercent + "%";
        //     percentFilled.fillAmount = percentSlider.value = item.defaultPercent / 100f;
        //     percentFilled.color = item.color;
        //     itemName.text = item.itemName;
        //     itemName.color = item.color;
        //     itemDescription.text = item.description;
        // }

        public void ForSlider(float sliderChange)
        {
            percent.text = Mathf.RoundToInt(sliderChange * 100f) + "%";
            percentFilled.fillAmount = sliderChange;
        }

        public void SaveSliderValue(float valueToSave)
        {
            _item.defaultPercent = Mathf.RoundToInt(valueToSave * 100f);
        }

        public void ChangePercent(int value)
        {
            _item.defaultPercent += value;
            _item.defaultPercent = Mathf.Clamp(_item.defaultPercent, 0, 100);
            percent.text = _item.defaultPercent + "%";
            StartCoroutine(ChangeLerpPercent());
        }

        private IEnumerator ChangeLerpPercent()
        {
            while (Math.Abs(percentFilled.fillAmount - _item.defaultPercent / 100f) > .03f)
            {
                percentFilled.fillAmount = Mathf.Lerp(percentFilled.fillAmount,
                    _item.defaultPercent / 100f, fillSpeed * Time.deltaTime);

                yield return null;
            }
        }

        public void GetPercent(string value)
        {
            if (value == "0")
            {
                _item.defaultPercent = 0;
                percent.text = _item.defaultPercent + "%";
                StartCoroutine(ChangeLerpPercent());
            }

            TryParse(value, out int number);
            Debug.Log(number);
            if (number != 0)
            {
                _item.defaultPercent = Mathf.Clamp(number, 0, 100);
                percent.text = _item.defaultPercent + "%";
                StartCoroutine(ChangeLerpPercent());
            }

            // percentField.text = "";
        }

        public void RemoveThemSelf()
        {
            Destroy(gameObject, .01f);
        }
    }
}