﻿using System;
using Mechanics;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Data_Stuff
{
    public class HoldButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        [SerializeField] private UnityEvent onHold;
        [SerializeField] private ItemSettings settings;

        private float _openFieldDelay;
        private float _timer;
        private bool _isPressed;

        private void Start() => 
            _openFieldDelay = settings.openFieldDelay;

        private void Update()
        {
            if (!_isPressed || onHold is null)
            {
                if (Math.Abs(_timer) > .01f)
                    _timer = 0;
                return;
            }

            _timer += Time.deltaTime;
            if (_timer >= _openFieldDelay)
            {
                _timer = 0;
                onHold.Invoke();
            }
        }
        
        public void OnPointerDown(PointerEventData eventData) => 
            _isPressed = true;

        public void OnPointerUp(PointerEventData eventData) => 
            _isPressed = false;
    }
}