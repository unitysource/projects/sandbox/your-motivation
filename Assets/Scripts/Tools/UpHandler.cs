﻿using Mechanics;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Data_Stuff
{
    public class UpHandler : MonoBehaviour, IPointerUpHandler
    {
        [SerializeField] private ItemSettings itemSettings;

        private Slider _slider;

        private void Awake() =>
            _slider = GetComponent<Slider>();

        public void OnPointerUp(PointerEventData eventData)
        {
            Debug.Log(_slider.value);
            itemSettings.SaveSliderValue(_slider.value);
        }
    }
}